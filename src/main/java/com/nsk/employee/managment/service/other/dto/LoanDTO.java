package com.nsk.employee.managment.service.other.dto;

public class LoanDTO {

	private String loanAccountNumber;
	private String loanApprovedDate;

	public LoanDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getLoanAccountNumber() {
		return loanAccountNumber;
	}

	public void setLoanAccountNumber(String loanAccountNumber) {
		this.loanAccountNumber = loanAccountNumber;
	}

	public String getLoanApprovedDate() {
		return loanApprovedDate;
	}

	public void setLoanApprovedDate(String loanApprovedDate) {
		this.loanApprovedDate = loanApprovedDate;
	}

}
