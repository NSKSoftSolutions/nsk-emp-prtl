package com.nsk.employee.managment.service.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nsk.employee.managment.service.business.services.EmployeeServices;
import com.nsk.employee.managment.service.domain.Employee;
import com.nsk.employee.managment.service.dto.EmployeeDTO;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeServices employeeServices;

	EmployeeDTO employeeDTO;

	@PostMapping(value = "/employee")
	public ResponseEntity<Object> addEmployee(@RequestBody EmployeeDTO employeeDTO) {
		employeeDTO = employeeServices.insertEmployee(employeeDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{employeeId}")
				.buildAndExpand(employeeDTO.getEmployeeId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@GetMapping(value = "/all-employees")
	public List<EmployeeDTO> getAllEmployee() {
		return employeeServices.getAllEmployees();
	}

	@GetMapping(value = "/employee/{email}")
	public Employee getEmployeeUsingEmail(@PathVariable String email) {
		return employeeServices.getEmployeeUsingEmail(email);
	}

	@PutMapping(value = "/employee")
	public ResponseEntity<Object> updateEmployee(@RequestBody EmployeeDTO employeeDTO) {
		employeeDTO = employeeServices.insertEmployee(employeeDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{employeeId}")
				.buildAndExpand(employeeDTO.getEmployeeId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@DeleteMapping(value = "/employee/{email}")
	public int deleteEmployeeUsingEmail(@PathVariable String email) {
		return employeeServices.deleteByEmail(email);
	}
	
	
}
