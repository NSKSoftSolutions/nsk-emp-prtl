package com.nsk.employee.managment.service.business.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.nsk.employee.managment.service.business.services.EmployeeServices;
import com.nsk.employee.managment.service.domain.Employee;
import com.nsk.employee.managment.service.dto.EmployeeDTO;
import com.nsk.employee.managment.service.repository.EmployeeRespository;

@Service
public class EmployeeServicesImpl implements EmployeeServices {

	@Autowired
	EmployeeRespository employeeRepository;
	Employee employee;

	@Override
	@Transactional
	public EmployeeDTO insertEmployee(EmployeeDTO employeeDTO) {
		employee = Employee.createEmployee(employeeDTO);
		employee = employeeRepository.saveAndFlush(employee);
		return EmployeeDTO.convertToEmployeeDTO(employee);
	}

	@Override
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employees = employeeRepository.findAll();

		return Employee.converToEmployeeDTOList(employees);
	}

	@Override
	public Employee getEmployeeUsingEmail(String email) {
		return employee = employeeRepository.findByEmail(email);

	}

	@Override
	@org.springframework.transaction.annotation.Transactional
	@Modifying
	public int deleteByEmail(String email) {
		return employeeRepository.deleteByEmail(email);
	}

}
