package com.nsk.employee.managment.service.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.nsk.employee.managment.service.dto.EmployeeDTO;
import com.nsk.employee.managment.service.other.dto.AddressDTO;
import com.nsk.employee.managment.service.other.dto.AssetDTO;
import com.nsk.employee.managment.service.other.dto.LoanDTO;
import com.nsk.employee.managment.service.other.dto.PassportDTO;

@Entity
public class Employee {
	@Id
	private long employeeId;
	private String firstName;
	private String lastName;
	private String email;
	private String doorNo;
	private String assetId;
	private String loanAccountNumber;
	private String passportNumber;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(String doorNo) {
		this.doorNo = doorNo;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getLoanAccountNumber() {
		return loanAccountNumber;
	}

	public void setLoanAccountNumber(String loanAccountNumber) {
		this.loanAccountNumber = loanAccountNumber;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Employee(long employeeId, String firstName, String lastName, String email) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + ", doorNo=" + doorNo + ", assetId=" + assetId + ", loanAccountNumber=" + loanAccountNumber
				+ ", passportNumber=" + passportNumber + "]";
	}

	public static Employee createEmployee(EmployeeDTO employeeDTO) {

		Employee employee = new Employee();
		employee.setEmployeeId(employeeDTO.getEmployeeId());
		employee.setFirstName(employeeDTO.getFirstName());
		employee.setLastName(employeeDTO.getLastName());
		employee.setEmail(employeeDTO.getEmail());
		employee.setDoorNo(employeeDTO.getDoorNo());
		employee.setAssetId(employeeDTO.getAssetId());
		employee.setLoanAccountNumber(employeeDTO.getLoanAccountNumber());
		employee.setPassportNumber(employeeDTO.getPassportNumber());
		return employee;
	}

	public static Employee converToEmployee(EmployeeDTO employeeDTO) {

		Employee employee = new Employee();
		employee.setEmployeeId(employeeDTO.getEmployeeId());
		employee.setFirstName(employeeDTO.getFirstName());
		employee.setLastName(employeeDTO.getLastName());
		employee.setEmail(employeeDTO.getEmail());
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setDoorNo(employeeDTO.getDoorNo());
		AssetDTO assetDTO = new AssetDTO();
		assetDTO.setAssetId(employeeDTO.getAssetId());
		LoanDTO loanDTO = new LoanDTO();
		loanDTO.setLoanAccountNumber(employeeDTO.getLoanAccountNumber());
		PassportDTO passportDTO = new PassportDTO();
		passportDTO.setPassportNumber(employeeDTO.getPassportNumber());
		return employee;
	}

	static EmployeeDTO employeeDTO;

	public static List<EmployeeDTO> converToEmployeeDTOList(List<Employee> employees) {
		List<EmployeeDTO> employeeListDTO = new ArrayList<EmployeeDTO>();
		for (Employee employee : employees) {
			employeeDTO = EmployeeDTO.convertToEmployeeDTO(employee);
			employeeListDTO.add(employeeDTO);
		}
		return employeeListDTO;
	}

}