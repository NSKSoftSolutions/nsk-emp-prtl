package com.nsk.employee.managment.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nsk.employee.managment.service.domain.Employee;

public interface EmployeeRespository extends JpaRepository<Employee, Long> {

	Employee findByEmail(String email);

	int deleteByEmail(String email);

}
