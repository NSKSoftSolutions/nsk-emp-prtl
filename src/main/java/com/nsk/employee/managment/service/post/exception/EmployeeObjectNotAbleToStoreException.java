package com.nsk.employee.managment.service.post.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class EmployeeObjectNotAbleToStoreException extends RuntimeException {

	public EmployeeObjectNotAbleToStoreException(String message) {
		super(message);
	}
}
