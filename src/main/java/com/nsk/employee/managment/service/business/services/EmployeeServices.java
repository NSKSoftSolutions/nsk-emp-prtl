package com.nsk.employee.managment.service.business.services;

import java.util.List;

import com.nsk.employee.managment.service.domain.Employee;
import com.nsk.employee.managment.service.dto.EmployeeDTO;

public interface EmployeeServices {

	EmployeeDTO insertEmployee(EmployeeDTO employeeDTo);

	List<EmployeeDTO> getAllEmployees();

	public Employee getEmployeeUsingEmail(String email);

	int deleteByEmail(String email);

}
