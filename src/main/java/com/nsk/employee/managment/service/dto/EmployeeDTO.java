package com.nsk.employee.managment.service.dto;

import com.nsk.employee.managment.service.domain.Employee;
import com.nsk.employee.managment.service.other.dto.AddressDTO;
import com.nsk.employee.managment.service.other.dto.AssetDTO;
import com.nsk.employee.managment.service.other.dto.LoanDTO;
import com.nsk.employee.managment.service.other.dto.PassportDTO;

public class EmployeeDTO {

	private long employeeId;
	private String firstName;
	private String lastName;
	private String email;
	private String doorNo;
	private String assetId;
	private String loanAccountNumber;
	private String passportNumber;
	private AssetDTO asset;
	private LoanDTO loan;
	private AddressDTO address;
	private PassportDTO passport;

	@Override
	public String toString() {
		return "EmployeeDTO [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", doorNo=" + doorNo + ", assetId=" + assetId + ", loanAccountNumber="
				+ loanAccountNumber + ", passportNumber=" + passportNumber + ", asset=" + asset + ", loan=" + loan
				+ ", address=" + address + ", passport=" + passport + "]";
	}

	public String getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(String doorNo) {
		this.doorNo = doorNo;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getLoanAccountNumber() {
		return loanAccountNumber;
	}

	public void setLoanAccountNumber(String loanAccountNumber) {
		this.loanAccountNumber = loanAccountNumber;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public AssetDTO getAsset() {
		return asset;
	}

	public void setAsset(AssetDTO asset) {
		this.asset = asset;
	}

	public LoanDTO getLoan() {
		return loan;
	}

	public void setLoan(LoanDTO loan) {
		this.loan = loan;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public PassportDTO getPassport() {
		return passport;
	}

	public void setPassport(PassportDTO passport) {
		this.passport = passport;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EmployeeDTO() {
		// TODO Auto-generated constructor stub
	}

	public static EmployeeDTO convertToEmployeeDTO(Employee employee) {

		EmployeeDTO employeeDTO = new EmployeeDTO();
		employeeDTO.setEmployeeId(employee.getEmployeeId());
		employeeDTO.setFirstName(employee.getFirstName());
		employeeDTO.setLastName(employee.getLastName());
		employeeDTO.setEmail(employee.getEmail());
		employeeDTO.setAssetId(employee.getAssetId());
		employeeDTO.setDoorNo(employee.getDoorNo());
		employeeDTO.setLoanAccountNumber(employee.getLoanAccountNumber());
		employeeDTO.setPassportNumber(employee.getPassportNumber());
		return employeeDTO;
	}

}
