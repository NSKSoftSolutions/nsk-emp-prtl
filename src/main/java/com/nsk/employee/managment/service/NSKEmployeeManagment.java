package com.nsk.employee.managment.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NSKEmployeeManagment {

	public static void main(String[] args) {
		SpringApplication.run(NSKEmployeeManagment.class, args);
	}

}
