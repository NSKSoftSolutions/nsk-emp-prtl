package com.nsk.employee.managment.service.other.dto;

public class AssetDTO {

	private String assetId;
	private String assetTaggedTo;
	private String assetName;
	private String assetTaggedDate;
	private String assetEndDate;
	private int noOfAssetsTagged;

	public AssetDTO() {
		// TODO Auto-generated constructor stub
	}
	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getAssetTaggedTo() {
		return assetTaggedTo;
	}

	public void setAssetTaggedTo(String assetTaggedTo) {
		this.assetTaggedTo = assetTaggedTo;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getAssetTaggedDate() {
		return assetTaggedDate;
	}

	public void setAssetTaggedDate(String assetTaggedDate) {
		this.assetTaggedDate = assetTaggedDate;
	}

	public String getAssetEndDate() {
		return assetEndDate;
	}

	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}

	public int getNoOfAssetsTagged() {
		return noOfAssetsTagged;
	}

	public void setNoOfAssetsTagged(int noOfAssetsTagged) {
		this.noOfAssetsTagged = noOfAssetsTagged;
	}

}
